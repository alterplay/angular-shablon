import { BrowserModule, Title }     from '@angular/platform-browser';
import { NgModule }                 from '@angular/core';
import { BrowserAnimationsModule }  from '@angular/platform-browser/animations';
import { HttpModule }               from '@angular/http';
import { AppRoutingModule }         from './app-routing.module';

import { AppComponent }             from './app.component';

import { AuthService }              from './services/auth.service';
import { DataBindingService }       from './services/data.binding.service';


@NgModule( {
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        AppRoutingModule
    ],
    providers: [
        DataBindingService,
        AuthService,
        Title
    ],
    bootstrap: [ AppComponent ]
} )

export class AppModule {}
