import { Injectable }           from '@angular/core';
import {
    CanActivate,
    Router,
    RouterStateSnapshot,
    ActivatedRouteSnapshot
}                               from '@angular/router';
import { Subscription }         from 'rxjs/Subscription';

import { DataBindingService }   from '../services/data.binding.service';
import { AuthService }          from '../services/auth.service';

@Injectable()

export class AuthGuard implements CanActivate {

    private userData: Object;
    private userDataSubscription: Subscription;

    constructor ( private router: Router,
                  private dataBindingService: DataBindingService,
                  private authService: AuthService ) {

        this.subscribeUserData();

    }

    private subscribeUserData (): void {

        this.userDataSubscription = this.dataBindingService.me.subscribe( me => {

            this.userData = me;

        } );

    };


    public canActivate ( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): any {

        let token = localStorage.getItem( 'auth_token' ),
            path = state.url.split( '?' ),
            url = path[ 0 ];

        if ( token ) {

            if ( url === '/auth' ) {

                this.router.navigate( [ '/' ] );

                return false;
            }

            this.dataBindingService.token.next( token );

        } else {

            if ( url !== '/auth' ) {

                this.router.navigate( [ 'auth' ] );
                return false;
            }

        }

        return true;

    }

}
