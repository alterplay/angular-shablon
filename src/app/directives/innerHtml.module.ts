import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { InnerHtmlDirective } from './innerhtml.directive';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [ InnerHtmlDirective ],
    exports: [InnerHtmlDirective]
})
export class InnerHtmlModule { }
