import {
    AfterViewInit,
    ElementRef,
    OnDestroy,
    Renderer2,
    ViewChild
}                               from '@angular/core';
import { Subscription }         from 'rxjs/Subscription';

import { MainSpinnerComponent } from '../spinner/spinner.component';

import { DataBindingService }   from '../../services/data.binding.service';

export class ContentCommonComponent extends MainSpinnerComponent implements OnDestroy, AfterViewInit {

    private mainSpinner: Subscription;

    protected view: Subscription;

    public isVisible: boolean;
    public viewSize: any;


    constructor ( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

        this.subscribeSpinnerVisibility();

        this.subscribeView();

    }

    private subscribeSpinnerVisibility () {

        this.mainSpinner = this.dataBindingService.mainSpinnerVisible.subscribe( ( visibleState: boolean ) => {

            this.isVisible = visibleState;

        } );

    }

    protected subscribeView (): void {

        this.view = this.dataBindingService.viewSize.subscribe( ( size: number ) => {

            this.viewSize = size;

        } );

    }

    protected unsubscribeAll (): void {

        this.view.unsubscribe();

        this.mainSpinner.unsubscribe();

    }


    public ngOnDestroy (): void {

        this.unsubscribeAll();

    }

    public ngAfterViewInit (): void {

    }

}
