import {Component, OnDestroy, OnInit}   from '@angular/core';
import { NavigationEnd, Router }        from '@angular/router';
import { Subscription }                 from 'rxjs/Subscription';

import { PreloaderComponent }           from '../preloader/preloader.component';

import { DataBindingService }           from '../../services/data.binding.service';

import { VisibilityChanged }            from '../../config/animations.config';

@Component({
    selector: 'errors',
    templateUrl: './errors.component.html',
    styleUrls: ['./errors.component.scss'],
    animations: [ VisibilityChanged ],
})
export class ErrorsComponent extends PreloaderComponent implements OnDestroy, OnInit {

    private routerSubscription: Subscription;

    public errorType: string;

    constructor( protected dataBindingService: DataBindingService,
                 private router: Router ) {

        super( dataBindingService );

        this.subscribeRouter();

    }

    private subscribeRouter(): void {

        this.routerSubscription = this.router.events.subscribe( event => {

            if ( event instanceof NavigationEnd ) {

                this.errorType = event.urlAfterRedirects.substr( 1 ).split( '/' )[ 0 ];

            }

        });

    }

    public ngOnDestroy (): void {

        this.routerSubscription.unsubscribe();

    }

    public ngOnInit (): void {

        setTimeout( () => {

            this.hidePreloader();

        });

    }
}
