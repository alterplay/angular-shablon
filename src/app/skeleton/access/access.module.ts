import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { ReactiveFormsModule }  from '@angular/forms';
import { AccessRoutingModule }  from './access-routing.module';

import { AccessComponent }      from './access.component';
import { LoginComponent }       from './login/login.component';

import { AuthGuard }            from '../../guards/auth.guard';


@NgModule( {
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AccessRoutingModule
    ],
    declarations: [
        LoginComponent,
        AccessComponent
    ],
    providers: [
        AuthGuard
    ]
} )
export class AccessModule {
}
