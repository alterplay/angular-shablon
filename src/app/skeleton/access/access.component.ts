import { AfterViewInit, Component, OnDestroy }  from '@angular/core';

import { PreloaderComponent }                   from '../preloader/preloader.component';

import { DataBindingService }                   from '../../services/data.binding.service';


@Component( {
    selector: 'access',
    templateUrl: './access.component.html',
    styleUrls: [ './access.component.scss' ]
} )
export class AccessComponent extends PreloaderComponent implements AfterViewInit, OnDestroy {

    constructor ( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

    }


    public ngAfterViewInit () {

        setTimeout( () => {

            this.hidePreloader();

        } );

    }

    public ngOnDestroy (): void {

    }

}
