import { Component, OnInit, Input }                         from '@angular/core';
import { Router }                                           from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators }  from '@angular/forms';

import { SpinnerComponent }                                 from '../../spinner/spinner.component';

import { DataBindingService }                               from '../../../services/data.binding.service';
import { AuthService }                                      from '../../../services/auth.service';

import { VisibilityChanged }                                from '../../../config/animations.config';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [ VisibilityChanged ],
})
export class LoginComponent extends SpinnerComponent implements OnInit {

    public sentFlag: boolean = false;
    public registerForm: FormGroup;
    public failureFlag: boolean = false;
    public failureText: string = '';

    constructor (
        private formBuilder: FormBuilder,
        private authService: AuthService,
        protected dataBindingService: DataBindingService,
        protected router: Router ) {

        super();
    }

    private createForm(): void {

        this.registerForm = this.formBuilder.group({
            login: new FormControl( 'test@mail.com', [ Validators.required, Validators.email ] ),
            password: new FormControl( '123', Validators.required )
        });

    }

    private getToken(): void {

        let curToken = localStorage.getItem( 'auth_token' );

        if ( curToken ) {

            this.router.navigate( [ '' ] );

        }
    }


    public ngOnInit(): void {

        this.getToken();
        this.createForm();

    }

    public setDefaultState(): void {

        this.failureFlag = false;
    }

    public submit( { value }: any ): void {

        this.showSpinner();

        this.authService.send( 'auth', value )
            .subscribe(
                data => {

                    let dataJson = data.json(),
                        token = dataJson.token;

                    console.log( 'authService', dataJson, token );

                    window.localStorage.setItem( 'auth_token', token );

                    this.dataBindingService.token.next( token );

                    this.dataBindingService.me.next( dataJson );

                    this.router.navigate( [ '/' ] );

                },
                ( error ) => {

                    switch ( error[ 'status' ] ) {

                        case 401:

                            this.hideSpinner();
                            this.failureText = error[ 'message' ];
                            this.failureFlag = true;

                            break;
                    }
                }
            );

    }

}
