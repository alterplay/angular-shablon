import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { DashboardRoutingModule }   from './dashboard-routing.module';

import { DashboardComponent }       from './dashboard.component';
import { DashboardPopupComponent } from './dashboard-popup/dashboard-popup.component';

@NgModule( {
    imports: [
        CommonModule,
        DashboardRoutingModule
    ],
    declarations: [
        DashboardComponent,
        DashboardPopupComponent
    ]
} )
export class DashboardModule {
}
