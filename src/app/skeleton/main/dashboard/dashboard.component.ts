import { Component, OnInit } from '@angular/core';

import { ContentCommonComponent } from '../../content-common/content-common.component';

import { DataBindingService } from '../../../services/data.binding.service';

@Component( {
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.scss' ]
} )
export class DashboardComponent extends ContentCommonComponent implements OnInit {

    constructor( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

    }

    ngOnInit () {

        this.hideSpinner();

    }

}
