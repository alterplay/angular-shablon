import { AfterViewInit, Component, OnDestroy }      from '@angular/core';

import { Subscription }                             from 'rxjs/Subscription';

import { NavigationEnd, NavigationStart, Router }   from '@angular/router';

import { MainSpinnerComponent }                     from '../spinner/spinner.component';

import { DataBindingService }                       from '../../services/data.binding.service';

import { SlideInLeft, VisibilityChanged }           from '../../config/animations.config';

@Component( {
    selector: 'main',
    templateUrl: './main.component.html',
    styleUrls: [ './main.component.scss' ],
    animations: [ VisibilityChanged, SlideInLeft ]
} )
export class MainComponent extends MainSpinnerComponent implements OnDestroy, AfterViewInit {

    private view: Subscription;
    private menu: Subscription;
    private mainSpinner: Subscription;
    private routerSubscription: Subscription;

    public isVisible: boolean = true;
    public viewSize: number;
    public menuVisible: boolean = false;

    constructor ( private router: Router,
                  protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

        this.subscribeRouter();

        this.subscribeView();

        this.subscribeMenuVisibility();

        this.subscribeSpinnerVisibility();

    }


    private subscribeRouter (): void {

        this.routerSubscription = this.router.events.subscribe( event => {

            if ( event instanceof NavigationEnd ) {

                if ( this.viewSize < 1 ) {

                    this.menuVisible = false;

                    this.dataBindingService.menuVisible.next( this.menuVisible );

                }

            }

            if ( event instanceof NavigationStart ) {

                this.showSpinner();

                if ( event[ 'url' ] === '/auth' ) {

                    this.dataBindingService.token.next( null );

                    localStorage.removeItem( 'auth_token' );

                }

            }

        } );

    }

    private subscribeSpinnerVisibility () {

        this.mainSpinner = this.dataBindingService.mainSpinnerVisible.subscribe( ( visibleState: boolean ) => {

            this.isVisible = visibleState;

        } );

    }

    private subscribeMenuVisibility () {

        this.menu = this.dataBindingService.menuVisible.subscribe( ( visibleState: boolean ) => {

            this.menuVisible = visibleState;

        } );

    }

    private subscribeView (): void {

        this.view = this.dataBindingService.viewSize.subscribe( ( size: number ) => {

            this.viewSize = size;

        } );

    }

    private unsubscribeAll () {

        this.menu.unsubscribe();

        this.mainSpinner.unsubscribe();

        this.view.unsubscribe();

        this.routerSubscription.unsubscribe();

    }

    public logOut (): void {



    }

    public showHideMenu (): void {

        this.menuVisible = !this.menuVisible;

        this.dataBindingService.menuVisible.next( this.menuVisible );

    }

    public ngAfterViewInit () {

        setTimeout( () => {

            this.hidePreloader();

        } );

    }

    public ngOnDestroy (): void {

        this.unsubscribeAll();

    }

}
