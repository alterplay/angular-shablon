import { NgModule }         from '@angular/core';
import { CommonModule }     from '@angular/common';

import { Item2Component }   from './item-2.component';

@NgModule( {
    imports: [
        CommonModule
    ],
    declarations: [
        Item2Component
    ]
} )
export class Item2Module {
}
