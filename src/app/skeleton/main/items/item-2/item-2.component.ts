import { Component, OnInit } from '@angular/core';

import { ContentCommonComponent } from '../../../content-common/content-common.component';

import { DataBindingService } from '../../../../services/data.binding.service';

@Component({
  selector: 'item-2',
  templateUrl: './item-2.component.html',
  styleUrls: ['./item-2.component.scss']
})
export class Item2Component extends ContentCommonComponent implements OnInit {

    constructor( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

    }

    ngOnInit () {

        this.hideSpinner();

    }

}
