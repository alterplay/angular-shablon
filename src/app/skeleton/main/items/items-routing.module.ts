import { NgModule }                     from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';
import { ItemComponent }                from './item/item.component';
import { Item2Component }               from './item-2/item-2.component';

const routes: Routes = [
    {
        path: '',
        component: ItemComponent
    },
    {
        path: 'item-2',
        component: Item2Component
    }
];

@NgModule({
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
})
export class ItemsRoutingModule { }
