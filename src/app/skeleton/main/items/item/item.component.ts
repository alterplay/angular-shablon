import { Component, OnInit }        from '@angular/core';

import { ContentCommonComponent }   from '../../../content-common/content-common.component';

import { DataBindingService }       from '../../../../services/data.binding.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: [ './item.component.scss' ]
})
export class ItemComponent extends ContentCommonComponent implements OnInit {

    constructor( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

    }

    ngOnInit () {

        this.hideSpinner();

    }

}
