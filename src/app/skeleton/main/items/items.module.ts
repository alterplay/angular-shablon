import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { ItemsRoutingModule }   from './items-routing.module';
import { ItemModule }           from './item/item.module';
import { Item2Module }          from './item-2/item-2.module';

@NgModule( {
    imports: [
        CommonModule,
        ItemModule,
        Item2Module,
        ItemsRoutingModule
    ],
    declarations: []
} )
export class ItemsModule {}
