import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { MainRoutingModule }    from './main-routing.module';
import { InnerHtmlModule }      from '../../directives/innerHtml.module';

import { MainComponent }        from './main.component';
import { MenuComponent }        from './menu/menu.component';

import { AuthGuard }            from '../../guards/auth.guard';


@NgModule( {
    imports: [
        CommonModule,
        MainRoutingModule,
        InnerHtmlModule
    ],
    declarations: [
        MainComponent,
        MenuComponent,
    ],
    providers: [
        AuthGuard
    ]
} )

export class MainModule {}
