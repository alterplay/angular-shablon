import { Component, OnInit } from '@angular/core';

import { ContentCommonComponent } from '../../content-common/content-common.component';

import { DataBindingService } from '../../../services/data.binding.service';

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent extends ContentCommonComponent implements OnInit {

    constructor( protected dataBindingService: DataBindingService ) {

        super( dataBindingService );

    }

    ngOnInit () {

        this.hideSpinner();

    }

}
