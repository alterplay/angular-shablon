
const domain = 'https://alty.co/assets/test_api';

//https://alty.co/assets/test_api/login.php
//https://alty.co/assets/test_api/auth.php

// login=test@mail.com
// password=123
// token=123

export const AuthRequests: Object = {
    domain: domain,
    auth: {
        url: '/login.php',
        method: 'get'
    }
};
