export const TitleConfig: Object = {
    '/': 'Dashboard',
    '/auth': 'Authorization',
    '/settings': 'Settings',
    '/items': 'Items',
    '/items/item-2': 'Items',
};
