import { Injectable }       from '@angular/core';
import { BehaviorSubject }  from 'rxjs/BehaviorSubject';

@Injectable()

export class DataBindingService {

    public token: BehaviorSubject<string> = new BehaviorSubject( null );
    public preloaderVisible: BehaviorSubject<boolean> = new BehaviorSubject( true );
    public mainSpinnerVisible: BehaviorSubject<boolean> = new BehaviorSubject( true );
    public me: BehaviorSubject<Object> = new BehaviorSubject( {} );
    public viewSize: BehaviorSubject<number> = new BehaviorSubject( 0 );
    public menuVisible: BehaviorSubject<boolean> = new BehaviorSubject( false );

}

