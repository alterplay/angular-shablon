import { Injectable }       from '@angular/core';
import { BehaviorSubject }  from 'rxjs/BehaviorSubject';

@Injectable()
export class OverlayService {

    public htmlTag = document.getElementsByTagName( 'HTML' )[ 0 ];

    public activePopup$: BehaviorSubject<any> = new BehaviorSubject( null );

    public addPopup ( popup ) {
        this.activePopup$.next( popup );
    }

    public closePopup () {
        this.activePopup$.next( false );
    }

    public setOverflowOverlay ( state: boolean ) {

        this.htmlTag[ 'style' ].overflow = '';

    }

}
